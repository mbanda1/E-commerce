package com.cyphertech.biashara.Fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.cyphertech.biashara.R;
import com.cyphertech.biashara.TopDeal.TopDeal_Three;


public class TopDeals extends Fragment {


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_top_deals, container, false);


        Button button = view.findViewById(R.id.letsgo);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent skipIntent = new Intent(v.getContext(), TopDeal_Three.class);
                v.getContext().startActivity(skipIntent);
            }
        });

        return view;
    }

//    private static final String URL_DATA2 = "https://biz-point.herokuapp.com/categories";
//
//    private static final String URL_DATA = "https://biz-point.herokuapp.com/brands";
//
//
//    private RecyclerView categories_recyclerView;
//    private RecyclerView.Adapter adapter;
//    private List <Categories_ItemObject> categories_List;
//
//
//    //for brands
//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.activity_top_deal__one, container, false);
//
//
//        categories_recyclerView = view.findViewById(R.id.linear_categories);
//        // categories_recyclerView = view.findViewById(R.id.linear_brands);
//        categories_List = new ArrayList <>();
//
//        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 1);
//        categories_recyclerView.setLayoutManager(mLayoutManager);
//        categories_recyclerView.addItemDecoration(new GridSpacingItemDecoration(1, 0, true));
//        categories_recyclerView.setItemAnimator(new DefaultItemAnimator());
//        categories_recyclerView.setAdapter(adapter);
//
//        loadUrlData();
//        return view;
//    }
//
////    @Override
////    protected void onCreate(Bundle savedInstanceState) {
////        super.onCreate(savedInstanceState);
////        setContentView(R.layout.activity_top_deal__one);
////
////        loadUrlData();
////    }
//
//    private void loadUrlData() {
//
//        // final ProgressDialog progressDialog = new ProgressDialog(this);
//        // progressDialog.setMessage("Loading...");
//        //  progressDialog.show();
//
//        StringRequest stringRequest = new StringRequest(Request.Method.GET,
//                URL_DATA2, new Response.Listener <String>() {
//            @Override
//            public void onResponse(String response) {
//
//                //    progressDialog.dismiss();
//
//                try {
//
//                    //JSONObject jsonObject = new JSONObject(response);
//                    JSONArray array = new JSONArray(response);
//
//                    //JSONArray array = jsonObject.getJSONArray(0);
//
//                    for (int i = 0; i < array.length(); i++) {
//
//                        JSONObject jo = array.getJSONObject(i);
//
//                        String id = jo.getString("_id");
//                        String pic = jo.getString("categoryPicture");
//                        String name = jo.getString("categoryName");
//
//                        String image_url = URL_DATA2 + '/' + id + '/' + pic;
//
//                        Categories_ItemObject developers = new Categories_ItemObject(name, image_url);
//
//
//                        categories_List.add(developers);
//
//                    }
//
//                    //adapter = new Topdeal_CustomAdapter(categories_List, getActivity().getApplicationContext());
//                    adapter = new Topdeal_CustomAdapter(categories_List, (FragmentActivity) getActivity().getApplicationContext());
//
//                    categories_recyclerView.setAdapter(adapter);
//
//                } catch (JSONException e) {
//
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                Toast.makeText(getActivity(), "Error" + error.toString(), Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
//        requestQueue.add(stringRequest);
//    }
//
//
//    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {
//
//        private int spanCount;
//        private int spacing;
//        private boolean includeEdge;
//
//        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
//            this.spanCount = spanCount;
//            this.spacing = spacing;
//            this.includeEdge = includeEdge;
//        }
//
//        @Override
//        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
//            int position = parent.getChildAdapterPosition(view); // item position
//            int column = position % spanCount; // item column
//
//            if (includeEdge) {
//                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
//                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)
//
//                if (position < spanCount) { // top edge
//                    outRect.top = spacing;
//                }
//                outRect.bottom = spacing; // item bottom
//            } else {
//                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
//                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
//                if (position >= spanCount) {
//                    outRect.top = spacing; // item top
//                }
//            }
//        }
//    }
}








