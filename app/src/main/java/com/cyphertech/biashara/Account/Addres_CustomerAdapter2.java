package com.cyphertech.biashara.Account;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.cyphertech.biashara.R;

import java.util.List;

public class Addres_CustomerAdapter2 extends RecyclerView.Adapter<Addres_CustomerAdapter2.ViewHolder>{

    // @Nullable View emptyView;

    private List<AccountObject> developersLists;
    private Context context;
     private int lastSelection = -1;


    public Addres_CustomerAdapter2(List<AccountObject> developersLists, Context context) {
        this.developersLists = developersLists;
        this.context = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public Addres_CustomerAdapter2.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_address, parent, false);
        return new Addres_CustomerAdapter2.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final AccountObject developersList1 = developersLists.get(position);
        holder.name.setText(developersList1.getName());
        holder.phone.setText(developersList1.getPhone());
        holder.address.setText(developersList1.getAddress());
        holder.region.setText(developersList1.getRegion());
        holder.station.setText(developersList1.getStation());

        holder.radioButton.setChecked(lastSelection == position);
    }


    @Override
    public int getItemCount() {
        return developersLists.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {



        TextView name, phone, address, region, station;
        Button Delete;
        RadioButton radioButton;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.address_name);
            phone = itemView.findViewById(R.id.address_phone);
            address = itemView.findViewById(R.id.address_address);
            region = itemView.findViewById(R.id.address_region1);
            station = itemView.findViewById(R.id.address_station1);

            Delete = itemView.findViewById(R.id.address_delete);

            radioButton = itemView.findViewById(R.id.stateSwitch);
            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelection = getAdapterPosition();
                    notifyDataSetChanged();

                    Toast.makeText(Addres_CustomerAdapter2.this.context, "Shipping Address set", Toast.LENGTH_SHORT).show();
                    //bundle.putString(NAME_, developersLists.get(getAdapterPosition()).getName());

                }
            });
        }

    }


}
